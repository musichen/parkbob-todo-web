### TODO WEB (WEB CLIENT,  FRONTEND) ###

todo-web
	(node.js )
	pure javascript
	can use a framework like AngularJS
	
can do:
	user reg
	user login/logout
	todo create (no todo delete/update) 
	todo  list (show, render, display) 
	
** Frontend App **
- Use Javascript (No coffeescript, typescript, etc for an easier evaluation on our side)
- You can use a framework like AngularJS
- The app should be capable of performing:
- User Registration
- User Login/Logout
- Todo creation (you don't need to implement todo delete or update)
- Todo visualization
- As long as you provide these features, you are free to structure the app in the way you prefer.
