import React, { Component } from 'react';
import logo from '../../img/logo.png';
// import './App.css';

class Header extends Component {
	render() {
		return (
				<div className="app-header">
					<img src={logo} className="app-logo" alt="logo" />
					<h2>Todo web APP</h2>
				</div>
		);
	}
}

export default Header;