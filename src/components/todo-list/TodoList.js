import React, {Component} from 'react';
import './TodoList.css';
import TodoItem from './TodoItem';

class TodoList extends Component {
	constructor(props) {
		super(props);

		this.state = {
			todos: []
		};

	}

	renderTodos = () => {
		let todos;

		if (this.props.todos) {
			if (this.props.todos.length > 0) {
				todos = this.props.todos.map((todo, i) =>
					<TodoItem {...todo} key={i} />
				);
			} else {
				todos = (
					<p className="info-text" >
						No todos added yet ...
						<br/> Enter some tasks in the input above and hit 'enter' to add it.
					</p>
				);
			}
		}
		return todos;
	};


	render() {

		let todoList;

		if (this.props.loggedIn) {
			todoList = (
				<div className="todo-list">
					{ this.renderTodos() }
				</div>
			);
		} else { /* case of not logged in user */
			todoList = (
				<div className="todo-list">
					<p style={{textAlign: 'center'}}> Welcome to TODO Web App. </p>
					<p style={{textAlign: 'center'}}> Please Login or Signup to start adding Todo notes.</p>
				</div>
			);
		}
		return todoList;
	}
}

export default TodoList;