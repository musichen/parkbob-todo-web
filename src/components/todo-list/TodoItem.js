import React, { Component } from 'react';
import './TodoItem.css';
import Checkbox from 'rc-checkbox';
import 'rc-checkbox/assets/index.css';
import deleteCross from './deleteCross.svg';
import editPencil from './editPencil.svg';

import http from '../../utilities/http';
import reactMixin from 'react-mixin';
import EventEmitterMixin from "react-event-emitter-mixin";
import eventStore from '../../utilities/eventStore';


class TodoItem extends Component {
	constructor(props) {
		super(props);
		this.state = {
			editMode: false,
			disabled: false,
			task: this.props.task,
			done: this.props.done
		};

	}

	componentDidUpdate(prevProps, prevState) {
			/* only update if  todo_ completion (checked) state changed */
		if (prevState.done !== this.state.done ) {
			console.log("did update  prestate.done: ", prevState.done, " this.state.done: ", this.state.done);

			/* update the  checked status of todo_ in db */
			http.put(`api/todo/edit/${this.props._id}`, { done: this.state.done }, true)
				.done((response, textStatus, jqXHR) => {
					/* emit event todo_ updated to update the App state */
					this.eventEmitter('emit', eventStore.todo.updated, response.todo);

					/* display notification about successfull  todo_ update (COMPLETION or UN-COMPLETION)  check / uncheck  */
					/* TODO_ checked as done */
					if (this.state.done) {
						window.notificationSystem.addNotification({
							title: "Task marked completed!",
							message: `Task has been marked as COMPLETED`,
							level: "success",
							position: "tc",
							autoDismiss: 2
						});
					} else { /* TODO_ is checked as undone */
						window.notificationSystem.addNotification({
							title: "Task marked back as UNDONE",
							message: `Task has been marked back as undone`,
							level: "success",
							position: "tc",
							autoDismiss: 2
						});
					}

				})
				.fail((jqXHR, textStatus, errorThrown) => {
					console.log("HTTP Request Failed");
				});
		}

	};

	/* on Task COMPLETE / UNCOMPLETE change  (checkbox change) UPDATE the complete status of the todo_ */
	onCheckboxChange = (e) => {
		this.setState({ done: e.target.checked });
		console.log("e.target.checked: ", e.target.checked, "this.state.done: ", this.state.done);

	};

	/*  perfrom TODO_ delete */
	onDelete = () => {
		http.del(`api/todo/${this.props._id}`, {}, true)
			.done((response, textStatus, jqXHR) => {
				console.log("HTTP Request Succeeded: " + jqXHR.status);
				console.log(response);
				/* emit event todo_ deleted to update the App state */
				this.eventEmitter('emit', eventStore.todo.deleted, response.todo);

				/* display notification about successfull new todo_ addition  */
				window.notificationSystem.addNotification({
					title: "Todo deleted!",
					message: `Todo task has been successfully deleted!`,
					level: "success",
					position: "tc",
					autoDismiss: 2
				});
			})
			.fail((jqXHR, textStatus, errorThrown) => {
				console.log("HTTP Request Failed");
			});
	};

	/* TODO_ EDIT AND UPDATE */

	/* handle edit pencil click to activate edit mode */
	onEdit = () => {
		// alert(' edit clicked! ');
		this.toggleEditMode();

	};

	/* detect ENTER button on keyboard was pressed  for fast TODO_ EDIT and UPDATE */
	detectEnterKeyPressedToUpdate = (e) => {
		if (e.charCode === 13 || e.keyCode === 13) {
			if (this.state.task.length > 2) {
				this.updateTodo();
			} else {
				window.notificationSystem.addNotification({
					title: "Too short todo input",
					message: `Todo should be at least 3 characters long.`,
					level: "warning",
					position: "tc",
					autoDismiss: 3
				});
			}
		}
	};

	updateTodo = () => {

		http.put(`api/todo/edit/${this.props._id}`, { task: this.state.task }, true)
			.done((response, textStatus, jqXHR) => {
				console.log("HTTP Request Succeeded: " + jqXHR.status);
				console.log(response);
				this.eventEmitter('emit', eventStore.todo.updated, response.todo);

			/* display notification about successfull new todo_ addition  */
			window.notificationSystem.addNotification({
				title: "Todo updated!",
				message: `Todo task has been successfully updated!`,
				level: "success",
				position: "tc",
				autoDismiss: 2
			});
			this.toggleEditMode();

		}).fail((jqXHR, textStatus, errorThrown) => {
				console.log("HTTP Request Failed");
			});

	};

	onDoubleClickTodoEdit = () => {
		//  switch to edit mode
		this.toggleEditMode();
	};

	onTodoTaskTextChange = (e) => {
		this.setState({ task: e.target.value });
	};

	toggleEditMode = () => {
		this.setState({
			editMode: !this.state.editMode,
		});
	};

	renderTodoTaskText = () => {
		let taskText;

		if (this.state.done) { // render  todo_ which marked/checked as completed
			taskText = ( <span onDoubleClick={this.onDoubleClickTodoEdit} className="todo-text"> <del> {this.state.task} </del> </span> );
		} else {
			taskText = ( <span onDoubleClick={this.onDoubleClickTodoEdit} className="todo-text"> {this.state.task} </span> );
		}
		return taskText;
	};

	render() {
		let todoItem;

		if (!this.state.editMode) { /* NORMAL MODE (JUST DISPLAY) */
			todoItem = (
				<div className="todo-item">
					<label className="todo-item-label" onDoubleClick={this.onDoubleClickTodoEdit} >
						<Checkbox className="todo-item-checkbox "
								  checked={this.state.done}
								  onChange={this.onCheckboxChange}
								  disabled={this.state.disabled}
						/>
						{ this.renderTodoTaskText() }
					</label>
					<img className="edit-pencil-button" src={editPencil} onClick={this.onEdit} alt="edit" />
					<img className="delete-cross-button" src={deleteCross} onClick={this.onDelete} alt="delete" />

				</div>
			);

		} else { /* EDIT MODE */
			todoItem = (
				<div className="todo-item">
						<input className="input-edit-todo-text" value={this.state.task} onChange={this.onTodoTaskTextChange} onKeyPress={this.detectEnterKeyPressedToUpdate} />
					<img className="edit-pencil-button" src={editPencil} onClick={this.onEdit} alt="edit" />
					<img className="delete-cross-button" src={deleteCross} onClick={this.onDelete} alt="delete" />
				</div>
			);
		}

		return todoItem;
	}
}
reactMixin(TodoItem.prototype, EventEmitterMixin);

export default TodoItem;
/*

 <button className="delete-todo-button"> </button>


 <div class="view">
 	<input type="checkbox" class="toggle" value="on">
 	<label>buy milk</label>
 	<button class="destroy"></button>
 </div>
*/


/*
round  checkbox

 <div class="container">
 	<div class="round">
	 <input type="checkbox" id="checkbox" />
 	<label for="checkbox"></label>
 	</div>
 </div>


**/