import React, { Component } from 'react';
import './EntryTodoField.css';

import $ from 'jquery';
import reactMixin from 'react-mixin';
import EventEmitterMixin from "react-event-emitter-mixin";
import storage from '../../utilities/storage';
import eventStore from '../../utilities/eventStore';
import getConfig from '../../utilities/getConfig';

class EntryTodoField extends Component {
/*	constructor(props) {
		super(props);
	} */

	componentDidMount() {
		/* focust in new todo_ input field when user logged in */
		if (this.props.loggedIn) $('#new-todo-input').focus();
	}
	componentDidUpdate() {
		/* focust in new todo_ input field when user logged in */
		if (this.props.loggedIn) $('#new-todo-input').focus();
	}


	/* detect ENTER button on keyboard was pressed  for fast todo adding */
	detectEnterKeyPressed = (e) => {
		if (e.charCode === 13 || e.keyCode === 13) {
			// this.addNewTodo();
			if ($('#new-todo-input').val().length > 2) {
				this.addNewTodo();
			} else {
				window.notificationSystem.addNotification({
					title: "Too short todo input",
					message: `Please enter at least 3 characters long todo.`,
					level: "warning",
					position: "tc",
					autoDismiss: 3
				})
			}
		}
	};

	addNewTodo = () => {
		// $('#new-todo-input').val()

		$.ajax({
			url: `${getConfig.apiUrl}/api/todo/add`,
			type: "POST",
			headers: {
				"Authorization": storage.get("jwt"),
				"Content-Type": "application/x-www-form-urlencoded; charset=utf-8"
			},
			contentType: "application/x-www-form-urlencoded",
			data: {
				"task": $('#new-todo-input').val().toString(),
			},
		})
			.done((response, textStatus, jqXHR) => {
				console.log("HTTP Request Succeeded: " + jqXHR.status);
				console.log(response);

				/* emit event signed up  to update the App state */
				this.eventEmitter('emit', eventStore.todo.created, response.todo);

				/* display notification about successfull new todo_ addition  */
				window.notificationSystem.addNotification({
					title: "Todo added!",
					message: `Todo task has been successfully added!`,
					level: "success",
					position: "tc",
					autoDismiss: 2
				});

				/* clear the currnt todo_ input to allow next todo_ input */
				$('#new-todo-input').val('');

			})
			.fail((jqXHR, textStatus, errorThrown) => {
				console.log("HTTP Request Failed");
			});

	};


	render() {
		let entryTodoField;

		if (this.props.loggedIn) {
			entryTodoField = (
				<div className="entry-todo-field-container">
					<input id="new-todo-input" className="new-todo-input" placeholder="enter what to do and hit 'enter' to add ..." onKeyPress={this.detectEnterKeyPressed} />
				</div>
			);
		} else { /* not logged in */
			entryTodoField = (
				<div className="entry-todo-field-container">
				</div>
				);
		}
		return entryTodoField;
	}
}

reactMixin(EntryTodoField.prototype, EventEmitterMixin);

export default EntryTodoField;