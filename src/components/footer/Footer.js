import React, { Component } from 'react';
import './Footer.css';

class Footer extends Component {
	render() {
		return (
			<div className="footer">
				<p>
					<strong> HOW TO USE: </strong>
				</p>
				<p>
					To edit a TODO: double click on it or click pencil icon
					<br/>
					To delete a TODO: click on red cross icon
				</p>
			</div>
		);
	}
}

export default Footer;