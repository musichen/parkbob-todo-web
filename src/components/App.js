import React, { Component } from 'react';
import './App.css';
import './index.css';

import Header from './header/Header';
import Topbar from './topbar/Topbar';
import EntryTodoField from './entry-todo-field/EntryTodoField';
import TodoList from './todo-list/TodoList';
import Footer from './footer/Footer';

import _ from 'lodash';
import $ from 'jquery';
import reactMixin from 'react-mixin';
import EventEmitterMixin from "react-event-emitter-mixin";
import eventStore from '../utilities/eventStore';
import NotificationSystem from "react-notification-system";
import storage from '../utilities/storage';
import getConfig from '../utilities/getConfig';

class App extends Component {
	constructor(props) {
		super(props);

		/* initialize logged in status by reading the local storage*/
		if (storage.get('user')) {
			this.state = {
				loggedIn: true,
				user: storage.get('user'),
				todos: []
			};
		} else {
			this.state = {
				loggedIn: false,
				user: "",
				todos: []
			};
		}

	}

	getTodosFromDb = () => {
		$.ajax({
			url: `${getConfig.apiUrl}/api/todos`,
			type: "GET",
			headers: {
				"Authorization": storage.get('jwt')
			},
		})
			.done((data, textStatus, jqXHR) => {
				console.log("HTTP Request Succeeded: " + jqXHR.status);
				console.log(data);
				this.setState({todos: data.todos});
			})
			.fail((jqXHR, textStatus, errorThrown) => {
				console.log("HTTP Request Failed");
			});
	};


	componentDidMount() {

		/* initialize user todos */
		this.getTodosFromDb();

		/* listen to event NEW todo_ CREATED and update the state of todo_ list to display change immediately   */
		this.eventEmitter('on', eventStore.todo.created, (newTodo) => {
			const todos = this.state.todos;
			todos.push(newTodo);
			this.setState({
				todos: todos
			});
		});


		/* listen to event NEW todo_ UPDATED and update the state of todo_ list to display change immediately   */
		this.eventEmitter('on', eventStore.todo.updated, (newTodo) => {
			const todos = this.state.todos;
			_.remove(todos, (todo) => {
				return todo._id === newTodo._id
			});
			todos.push(newTodo);
			this.setState({
				todos: todos
			});
		});


		/* listen to event  todo_ DELETED and update the state of todo_ list to display change immediately   */
		this.eventEmitter('on', eventStore.todo.deleted, (deletedTodo) => {
			const todos = this.state.todos;
			_.remove(todos, (todo) => {
				return todo._id === deletedTodo._id
			});
			this.setState({
				todos: todos
			});
		});

		window.notificationSystem = this.refs.notificationSystem;
		this.eventEmitter('on', eventStore.auth.loggedIn, (loggedInUserEmail) => {
			this.setState({
				loggedIn: true,
				user: loggedInUserEmail
			});
			/* get current loggedIn user todos */
			this.getTodosFromDb();
		});

		this.eventEmitter('on', eventStore.auth.signedUp, (signedUpUserEmail) => {
			this.setState({
				loggedIn: true,
				user: signedUpUserEmail
			});
		});

		this.eventEmitter('on', eventStore.auth.loggedOut, () => {
			this.setState({
				loggedIn: false,
				user: "",
				todos: []
			});
		});

	}





	render() {
		const notificationSystemStyles = {
			NotificationItem: {
				DefaultStyle: {
					marginRight: '100px'
				}
			}
		};

		return (
			<div>
				<div className="app">
					<Header />
					<Topbar loggedIn={this.state.loggedIn} user={this.props.user} />
					<EntryTodoField loggedIn={this.state.loggedIn}  />
					<TodoList loggedIn={this.state.loggedIn} todos={this.state.todos} />
					<Footer />
				</div>
				<NotificationSystem ref="notificationSystem" style={notificationSystemStyles} />
			</div>
		);
	}
}

reactMixin(App.prototype, EventEmitterMixin);

export default App;
