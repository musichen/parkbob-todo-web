import React, { Component } from 'react';
import './Topbar.css';
import storage from '../../utilities/storage';
import eventStore from '../../utilities/eventStore';
import reactMixin from 'react-mixin';
import EventEmitterMixin from "react-event-emitter-mixin";

import Avatar from 'react-avatar';
import LoginSignupButtonAndDialog from './login-signup-dialog/LoginSignupButtonAndDialog';

class Topbar extends Component {
/*	constructor(props, context) {
		 super(props, context);
	}*/

	componentDidMount() {

	}

	onLogout = () => {
		/* remove the jwt from the local storage */
		storage.clear();

		/* emit event logged out  to update the App state */
		this.eventEmitter('emit', eventStore.auth.loggedOut);

		/* display notification about successfull logout */
		window.notificationSystem.addNotification({
			title: "Logged Out",
			message: `Successfully logged out!`,
			level: "success",
			position: "tc",
			autoDismiss: 3
		});
	};

	render() {
		let topbar;

		if (this.props.loggedIn) { /* logged in */
			topbar = (
				<div className="topbar">
					<div className="profile-bar">
						<div className="ava-container">
							<Avatar name={storage.get('user')} round={true} size={40} textSizeRatio={1} />
						</div>
						<div className="username-text">
							{ storage.get('user') }
						</div>
						<div className="btn-logout" onClick={this.onLogout}> logout </div>
					</div>

				</div>
			);
		} else { /*  not logged in */
			topbar = (
				<div className="topbar">
					<LoginSignupButtonAndDialog />
				</div>
			);
		}
		return topbar;
	}
}

reactMixin(Topbar.prototype, EventEmitterMixin);

export default Topbar;


