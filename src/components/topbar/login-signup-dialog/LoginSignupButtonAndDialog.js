import 'rc-dialog/assets/index.css';
import React from 'react';
import Dialog from 'rc-dialog'
import './LoginSignupButtonAndDialog.css';

import storage from '../../../utilities/storage';
import $ from 'jquery';
import reactMixin from 'react-mixin';
import EventEmitterMixin from "react-event-emitter-mixin";
import eventStore from '../../../utilities/eventStore';
import http from '../../../utilities/http';

class LoginSignupButtonAndDialog extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			visible: false,
			currentScreen: "login",
			destroyOnClose: true,
			center: false,
			email: "",
			password: ""
		};

	}

	componentDidMount() {

	}

	onClick = (e) => {
		this.setState({
			mousePosition: {
				x: e.pageX,
				y: e.pageY,
			},
			visible: true,
		});
	};

	onClose = (e) => {
		// console.log(e);
		this.setState({
			visible: false,
		});
	};

	onDestroyOnCloseChange = (e) => {
		this.setState({
			destroyOnClose: e.target.checked,
		});
	};

	center = (e) => {
		this.setState({
			center: e.target.checked,
		});
	};

	/* SWITCH SCREENS on button click between LOGIN and SIGNUP */
	goToLoginScreen = () => {
		this.setState({
			currentScreen: "login"
		});
	}

	goToSignupScreen = () => {
		this.setState({
			currentScreen: "signUp"
		});
	};

	/* detect ENTER button on keyboard was clicked  for fast convenitent quick submit */
	detectEnterKeyPressed = (e) => {
		if (e.charCode === 13 || e.keyCode === 13) {
			if (this.state.currentScreen === "login") this.onLoginSubmitted();
			if (this.state.currentScreen === "signUp") this.onSignupSubmitted();
		}
	};

	/* HANDLE LOGIN */

	onLoginSubmitted = () => {
		http.post(`api/auth`, { "email": $('#email').val(), "password": $('#password').val() }, true)
			.done((response, textStatus, jqXHR) => {
				/* set the jwt to the local storage */
				storage.set({ jwt: response.token });
				const user = response.useremail;
				storage.set({ user });

				/* emit event logged in  to update the App state */
				this.eventEmitter('emit', eventStore.auth.loggedIn, response.useremail);

				/* close the dialog window and destroy input */
				$('body > div:nth-child(4) > div > div.rc-dialog-wrap > div > div.rc-dialog-content > button > span').click();

				/* display notification about successfull login */
				window.notificationSystem.addNotification({
					title: "Logged In",
					message: `Successfully logged in as ${response.useremail}`,
					level: "success",
					position: "tc",
					autoDismiss: 3
				});

			})
			.fail((jqXHR, textStatus, errorThrown) => {
				console.log("Authentification  Failed");
				console.log(errorThrown);
				console.log(textStatus);
				window.notificationSystem.addNotification({
					title: "Login failed",
					message: `Incorrect email or password. Please re-check and try again.`,
					level: "error",
					position: "tc",
					autoDismiss: 3
				});
			});
	};

	performAuthentication = (login, password) => {
		http.post(`api/auth`, { "email": login, "password": password }, true)
			.done((response, textStatus, jqXHR) => {
				storage.set({ jwt: response.token });
				storage.set({ user: response.useremail });
			})
			.fail((jqXHR, textStatus, errorThrown) => {
				console.log("Authentification  Failed", textStatus, errorThrown);
			});
	};

	/* HANDLE SIGNUP */

	onSignupSubmitted = () => {
		http.post(`api/signup`, { "email": $('#email').val(), "password": $('#password').val() }, true)
			.done((response, textStatus, jqXHR) => {

				/* emit event signed up  to update the App state */
				this.eventEmitter('emit', eventStore.auth.signedUp, response.useremail);

				/* authenticate user immediately after signup */
				const email = $('#email').val();
				const ps = $('#password').val();

				setTimeout(() => {
					this.performAuthentication(email, ps, () => {
						this.eventEmitter('emit', eventStore.auth.loggedIn, response.useremail);
					});
				}, 500);

				/* close the dialog window and destroy input */
				$('body > div:nth-child(4) > div > div.rc-dialog-wrap > div > div.rc-dialog-content > button > span').click();

				/* display notification about successfull login */
				window.notificationSystem.addNotification({
					title: "Signed up!",
					message: `Welcome to TODO Web App! You are signed up and logged in as ${response.useremail}`,
					level: "success",
					position: "tc",
					autoDismiss: 5
				});
			})
			.fail((jqXHR, textStatus, errorThrown) => {
				console.log("HTTP Request Failed");
			});
	};

	/* LOGIN / SIGNUP INPUT FIELD  HANDLING */
	onEmailIpnutChange = (event) => {
		event.preventDefault();
		this.setState({
			email: event.target.value
		});

	};

	onPasswordInputChange = (event) => {
		event.preventDefault();
			/* TODO use listeting to pass input for validations ... to be done */
		this.setState({
			password: event.target.value
		});

		/* detect  ENTER pressed */
/*		if (event.keyCode === 13) {
			// $("#btn-submit-login-signedUp").click();
			// alert('enter clicked');
		}*/
	};

	/* SIGNUP INPUT FIELD HANDLING */


	render() {
		let dialog;

		if (this.state.visible || !this.state.destroyOnClose) {
			const style = {
				width: 320,
				height: 480
			};
			let wrapClassName = '';
			if (this.state.center) {
				wrapClassName = 'center';
			}

			let currentScreen;
			if (this.state.currentScreen === "login") { /* login screen active */
				currentScreen = (
					<div className="login-block-container">

						<div className="login-switcher">
							<div className="login-switch" onClick={this.goToLoginScreen}> login </div>
							<div className="signup-switch" onClick={this.goToSignupScreen}> signup </div>
						</div>

						<input id="email" className="login-signup-input"  type="text" value={this.state.name} onChange={this.onEmailIpnutChange}  placeholder="email"  />
						<input id="password" className="login-signup-input" type="password" value={this.state.name} onChange={this.onPasswordInputChange} onKeyPress={this.detectEnterKeyPressed} placeholder="password"  />
						<div className="submit-btn" onClick={this.onLoginSubmitted} id="btn-submit-login-signup"> Login </div>
					</div>
				);
			} else {  /* signedUp screen active */
				currentScreen = (
					<div className="login-block-container">

						<div className="login-switcher">
							<div className="login-switch" onClick={this.goToLoginScreen}> login </div>
							<div className="signup-switch" onClick={this.goToSignupScreen}> signup </div>
						</div>

						<input id="email" className="login-signup-input" type="text" value={this.state.name} onChange={this.onEmailIpnutChange}  placeholder="enter your email"  tabIndex="-1" />
						<input id="password" className="login-signup-input" type="password" value={this.state.name} onChange={this.onPasswordInputChange} onKeyPress={this.detectEnterKeyPressed} placeholder="enter your password" />
						<div className="submit-btn" onClick={this.onSignupSubmitted}> Signup </div>
					</div>
				);
			}

			dialog = (
				<Dialog
					visible={this.state.visible}
					wrapClassName={wrapClassName}
					animation="zoom"
					maskAnimation="fade"
					onClose={this.onClose}
					style={style}
					mousePosition={this.state.mousePosition}
				>
					{currentScreen}

				</Dialog>
			);
		}


		return (
			<div>
				<style>
					{
						`.center {
						  display: flex;
						  align-items: center;
						  justify-content: center;
						}`
					}
				</style>

					<button
						className="login-signup-dialog-trigger-button"
						onClick={this.onClick}
					>
						login &nbsp; | &nbsp; signup
					</button>

				{dialog}
			</div>
		);
	}
}

reactMixin(LoginSignupButtonAndDialog.prototype, EventEmitterMixin);

export default LoginSignupButtonAndDialog;
