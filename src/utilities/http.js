// import _ from "lodash";
import storage from "./storage";
import config from "./getConfig";
import $ from "jquery";

const defaultHeaders = () => {
	const jwtToken = storage.get("jwt");

	let headers;

	if (jwtToken) {
		headers = {
			"Authorization": jwtToken,
			"Content-Type": "application/x-www-form-urlencoded; charset=utf-8"
		};
	} else {
		headers = {
			"Content-Type": "application/x-www-form-urlencoded; charset=utf-8"
		};
	}

	return headers;
};


const get = (url, async) =>
	$.ajax({
		url: `${config.apiUrl}/${url}`,
		type: "GET",
		headers: defaultHeaders(),
		async: async
	});


const post = (url, data, async) =>
	$.ajax({
		url: `${config.apiUrl}/${url}`,
		type: "POST",
		headers: defaultHeaders(),
		data: data,
		async: async
	});

const put = (url, data, async) =>
	$.ajax({
		url: `${config.apiUrl}/${url}`,
		type: "PUT",
		headers: defaultHeaders(),
		data: data,
		async: async
	});

const patch = (url, data, async) =>
	$.ajax({
		url: `${config.apiUrl}/${url}`,
		type: "PATCH",
		headers: defaultHeaders(),
		data: data,
		async: async
	});

const del = (url, data, async) =>
	$.ajax({
		url: `${config.apiUrl}/${url}`,
		type: "DELETE",
		headers: defaultHeaders(),
		data: data,
		async: async
	});

export default { get, post, put, patch, del };
