/* Events Store to communicate between components avoiding event names clashing */

module.exports = {

	auth: {
		error: 'auth.error',
		loggedIn: 'auth.loggedIn',
		loggedOut: 'auth.loggedOut',
		signedUp: 'auth.singedUp'
	},
	todo: {
		error: 'todo.error',
		created: 'todo.created',
		updated: 'todo.updated',
		deleted: 'todo.deleted'
	}

};
