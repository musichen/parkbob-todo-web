import _ from "lodash";

const ls = window.localStorage;

const set = (obj) => {
	_.forEach(obj, (value, key) => {
		ls.setItem(key, JSON.stringify(value));
	});
};

/*const get = (key) => {
	return ls.getItem(key);
};*/

const get = (key) =>
 JSON.parse(ls.getItem(key));

const remove = (key) =>
	ls.removeItem(key);

const clear = () =>
	ls.clear();

export default { set, get, remove, clear };
