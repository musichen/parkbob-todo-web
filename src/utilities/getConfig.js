const NODE_ENV = process.env.NODE_ENV;
if (!NODE_ENV) {
	throw new Error(
		'The NODE_ENV environment variable is required but was not specified.'
	);
}

/* configure the backend API HOST AND PORT */
/* development */
const developmentApiUrl = '127.0.0.1:1234';
/* test */
const testApiUrl = '127.0.0.1:1234';
/* production */
const productionApiUrl = '127.0.0.1:1234';
/* raspberry Pi deployment on remote server musichen.noip.me */
const raspberryPiUrl = 'musichen.noip.me:1234';

let apiUrl;
switch(NODE_ENV) {
	case 'production':
		apiUrl = productionApiUrl;
		break;
	case 'development':
		apiUrl = developmentApiUrl;
		break;
	case 'rpi':
		apiUrl = raspberryPiUrl;
		break;
	case 'test':
		apiUrl = testApiUrl;
		break;
	default:
		apiUrl = developmentApiUrl;
}

module.exports = {
	apiUrl: `http://${apiUrl}`
};
